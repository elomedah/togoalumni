import React, { Component, Fragment } from 'react'
import Navigation from './components/HomePage/navigation';
import Body from './components/HomePage/Body';
import Profil from './components/Profil/profil';
import ProfilSearch from './components/Search/profilSearchResult/profil';
import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import Search from './components/Search/search';
import AddEtude from './components/Etude/AddEtude';
import Updateprofil from './components/Profil/Updateprofil';
import AddExperience from './components/Experience/AddExperience'
import Validation from './components/Validation'
import ForgetPassword from './components/ForgetPassword/indexPw'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Snackbar from "./components/Snackbar"
import UpdateEtude from "./components/Etude/UpdateEtude";
import UpdateExperience from "./components/Experience/UpdateExperience";

export class App extends Component {
  constructor() {
    super();

    this.state = {
      loggedInStatus: "NOT_LOGGED_IN",
      user: {}
    }
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  handleLogin(data) {
    this.setState({
      loggedInStatus: "LOGGED_IN",
      user: data
    })
  }
  handleLogout() {
    this.setState({
      loggedInStatus: "NOT_LOGGED_IN",
      user: {}
    })
    localStorage.clear();

  }

  render() {
    return (
      <Router>
        <div>
          <Snackbar />

          <Route exact
            path={""}
            render={props => (
              <Navigation {...props} handleLogout={this.handleLogout} loggedInStatus={this.state.loggedInStatus} />
            )}>
          </Route>
          <Route exact path="/"
            render={props => (
              <Body {...props} handleLogin={this.handleLogin} loggedInStatus={this.state.loggedInStatus} />
            )} />
          <Route exact
            path={"/login"}
            render={props => (
              <SignIn {...props} handleLogin={this.handleLogin} loggedInStatus={this.state.loggedInStatus} />
            )}>
          </Route>
          <Route exact
            path={"/profil"}
            render={props => (
              <Profil></Profil>
            )}>
          </Route>

          

          <Route exact
            path={"/search_profil/:id"}
            render={props => (
              <ProfilSearch></ProfilSearch>
            )}>
          </Route>
          <Route exact
            path={"/validation"}
            render={props => (
              <Validation {...props} handleLogout={this.handleLogout} loggedInStatus={this.state.loggedInStatus} />
            )}>
          </Route>
          <Route exact path="/signup" component={SignUp}></Route>
          <Route exact path="/search" component={Search}></Route>
          <Route exact path="/addetude" component={AddEtude}></Route>
          <Route exact path="/updateprofil" component={Updateprofil}></Route>
          <Route exact path="/updateetude/:id" component={UpdateEtude}></Route>
          <Route exact path="/updateexperience/:id" component={UpdateExperience}></Route>
          <Route exact path="/addexperience" component={AddExperience}></Route>
          <Route exact path="/forgetpassword" component={ForgetPassword}></Route>

        </div>
      </Router>
    )
  }
}

export default App;
