import axios from "axios";

export const REACT_APP_API_URL = 'http://51.158.117.13:8081';

//export const REACT_APP_API_URL = 'http://127.0.0.1:8000';


export default axios.create({
  baseURL: REACT_APP_API_URL + "/api/membership/profile",
  headers: {
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
});
