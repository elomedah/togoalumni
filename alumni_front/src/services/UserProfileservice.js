import http from "../http-common";

// Update and Get profile  

  const updateProfile = (id, data) => {
    return http.put(`/${id}`, data);
  };
  const getProfile = () => {
    return http.get(`/`);
  };

// CRUD for etude

  const getEtude = id => {
    return http.get(`/etudes/${id}`);
  };
  const createEtude = data => {
    return http.post("/etudes", data);
  };
  const updateEtude = (id, data) => {
    return http.patch(`/etudes/${id}`, data);
  };
  const removeEtude = id => {
    return http.delete(`/etudes/${id}`);
  };
  
// CRUD for experience

  const getExperience = id => {
    return http.get(`/experience_professionelle/${id}`);
  };
  const createExperience = data => {
    return http.post("/experience_professionelle", data);
  };
  const updateExperience = (id, data) => {
      return http.put(`/experience_professionelle/${id}`, data);
  };
  const removeExperience = id => {
    return http.delete(`/experience_professionelle/${id}`);
  };
  
  

  
  
  export default {
    getProfile,
    updateProfile,
    getEtude,
    createEtude,
    updateEtude,
    removeEtude,
    getExperience,
    createExperience,
    updateExperience,
    removeExperience,
    
  };