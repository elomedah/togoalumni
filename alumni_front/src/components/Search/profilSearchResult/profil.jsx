import React, { Component } from 'react'

import AboutMe from './AboutMe';
import Zoom from 'react-reveal/Zoom'
import BackgroundImagePage from './begins/begins'
import Resume from './resume'


export class ProfilSearch extends Component {
  
  
  
  render() {

    return (
      <div>

        <BackgroundImagePage></BackgroundImagePage>
        <Zoom>
        <AboutMe></AboutMe>
        <Resume></Resume>
        </Zoom>
      </div>
    )
  }
}

export default ProfilSearch
