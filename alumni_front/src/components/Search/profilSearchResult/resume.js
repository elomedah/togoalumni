import React from "react";
import {Grid,Box ,Typography} from '@material-ui/core'
import Fade from 'react-reveal/Fade';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import axios from 'axios';
import {useState, useEffect} from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import {REACT_APP_API_URL} from "../../../http-common";



const useStyles = makeStyles((theme)=>({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  margin: {
    marginBottom: "5px",
    marginLeft:"-8px",
  },
}));


export default function  Resume() {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    let profileId  = useParams();
    let id = profileId.id;

    function FormRow() {
      const [resume,setresume] =useState({pays:'', phone: '',statut: '', user:{}, studies:{}, pro_career:{}})
      const studiesList = Array.from(resume.studies);
      const jobsList = Array.from(resume.pro_career);

      function DataFetching2() {
    
        useEffect(() => {
          axios.get (REACT_APP_API_URL +'/api/search/profile/'+id)
          .then(res=> {
          setresume(res.data);
          })
          .catch(err =>{console.log(err)})
    
    },[])}
    const studiesListEelements = studiesList.map((study) => (
      <Fade left key={study.id}>
    <Box   fontWeight="fontWeightLight" m={2} >
    <Card className={classes.card}>
      <CardContent>
        
        <Typography variant="h5" component="h2">
        {study.diplome}
        
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
        {study.university} / {study.from_date} - {study.to_date}
        </Typography>
        <Typography variant="body2" component="p">
        {study.description}
          <br />
          
        </Typography>
      </CardContent>
    </Card>
            </Box> 
    </Fade>
      ))
        return (
          DataFetching2(),
          <React.Fragment>


            <Grid item xs={12}  >
            
        <Box fontWeight="fontWeightLight" m={2} >
                <Typography variant="h5" color="primary" component="p"  >
                ETUDES
                </Typography>        
        </Box>  
    
        {studiesListEelements}
    
            
        </Grid>
          </React.Fragment>
        );
      }
 
      function FormRowTwo() {
        const [resume,setresume] =useState({pays:'', phone: '',statut: '', user:{}, studies:{}, pro_career:{}})
       const studiesList = Array.from(resume.studies);
      const jobsList = Array.from(resume.pro_career);
      function DataFetching2() {
    
    useEffect(() => {
      axios.get (REACT_APP_API_URL +'/api/search/profile/'+id)
      .then(res=> {
      setresume(res.data);
      })
      .catch(err =>{console.log(err)})
    
    },[])}
    const jobsListEelements = jobsList.map((job) => (
      <Fade right key={job.id}>
    <Box   fontWeight="fontWeightLight" m={2} >
    <Card className={classes.card}>
      <CardContent>
        
        <Typography variant="h5" component="h2">
        {job.job_position}
        
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
        {job.place} / {job.from_date} - {job.to_date}
        </Typography>
        <Typography variant="body2" component="p">
        {job.description}
          <br />
          
        </Typography>
      </CardContent>
    </Card>
            </Box> 
    </Fade>
    ))
        return (
          DataFetching2(),
          <React.Fragment>
            <Grid item xs={12}  >
            
        <Box fontWeight="fontWeightLight" m={2} >
                <Typography variant="h5" color="primary" component="p" >
                EXPERIENCES PROFESSIONNELLES
                </Typography>
            </Box>  
    {jobsListEelements}
            
            
        </Grid>
          </React.Fragment>
        );
      }

 
 return (
 <div>
    <Fade right>
        
      <Grid  
            container
            direction="column"
            justify="space-between"
            alignItems="center"
          >
          <Grid >
        
                <Box fontWeight="fontWeightLight" m={2} >      
                     <Typography   variant="h2"  component="p" color="primary" >
                           RESUME
                    </Typography>
                </Box>
                </Grid>
                <Grid>
                <Box fontWeight="fontWeightLight" m={4} >      
                <Typography variant="h6" color="textSecondary" component="p"  >
                
                </Typography>
                </Box>
               
          </Grid>
      </Grid>

    </Fade>

      <Grid 
              container 
              spacing={6}
            >
            <Grid container item xs={12} md={6}  spacing={2}>
              <FormRow></FormRow>
            </Grid>
        
            <Grid container item  xs={12} md={6} spacing={2}  >
              <FormRowTwo> </FormRowTwo>
            </Grid>
        
      </Grid>
  </div>
      )
};
