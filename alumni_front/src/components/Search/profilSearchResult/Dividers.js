import React from 'react';
import './dividers.css';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import WorkIcon from '@material-ui/icons/Work';
import Divider from '@material-ui/core/Divider';
import {useState, useEffect} from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import CallIcon from '@material-ui/icons/Call';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import CakeIcon from '@material-ui/icons/Cake';
import PublicIcon from '@material-ui/icons/Public';
import SchoolIcon from '@material-ui/icons/School';
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import {REACT_APP_API_URL} from "../../../http-common";


const useStyles = makeStyles(theme => ({
  root: {
    
    width: '100%',
    //maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function InsetDividers() {
  const classes = useStyles();
  
  let profileId  = useParams();
  let id = profileId.id;
  const dispatch = useDispatch();
  
  const [posts,setPosts] =useState({pays:'', phone: '',statut: '', date_naissance:'', domaine:'', user:{}})

  

  function DataFetching() {
    
    useEffect(() => {
      axios.get (REACT_APP_API_URL +'/api/search/profile/'+id)
      .then(res=> {console.log(res);
        
      setPosts(res.data);
      
      
      })
      .catch(err =>{console.log(err)})
    
    },[])};
   
    
    
  return (
    
    DataFetching(),
    
    <List className={classes.root}>
      <Grid container spacing={1}>
      <Grid item xs={12} md={6}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <AccountCircleIcon />
          </Avatar>
        </ListItemAvatar>
        
        <ListItemText primary="NOM : " secondary={posts.user.last_name} />
      </ListItem>
      <Grid item xs={6}></Grid>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <AccountCircleIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="PRÉNOM :" secondary={posts.user.first_name} />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <AlternateEmailIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="EMAIL :" secondary={posts.user.email} />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <CakeIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="DATE OF BIRTH :" secondary={posts.date_naissance} />
      </ListItem>
      </Grid>
      <Grid item xs={12} md={6}>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <CallIcon />
          </Avatar>
        </ListItemAvatar>
        
        <ListItemText primary="TELEPHONE : " secondary={posts.phone} />
      </ListItem>
     
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <PublicIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="PAYS :" secondary={posts.pays} />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <SchoolIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="STATUT :" secondary={posts.statut}/>
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <WorkIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="DOMAINE :" secondary={posts.domaine} />
      </ListItem>
      </Grid>
      </Grid>
    </List>

  );
}