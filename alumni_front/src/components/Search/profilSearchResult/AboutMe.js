import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {useState, useEffect} from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Dividers from './Dividers';
import { useParams } from "react-router-dom";
import {REACT_APP_API_URL} from "../../../http-common";



const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: 'left',
    fontWeight:"fontWeightBold" ,
    fontSize:"24px",
  },
  button: {
    margin: theme.spacing(1),
    padding:'43px',
    fontSize: '72px',
    '&:hover': {
      background: "#3f51b5" ,
      color:"white",
    }
  },
  head:{
    //marginTop: theme.spacing(12),
    fontSize: '45px',
    fontWeight: 'Medium',
    variant:"h2",
  },
  margin: {
    marginBottom: "7px",
  },
}));


export default function AboutMe() {
  const classes = useStyles();
  let profileId  = useParams();
  let id = profileId.id;
  
  function FormRow() {
    const [posts,setPosts] =useState({pays:'', phone: '',statut: '',description:'', user:{}})
    function DataFetching() {
    
    useEffect(() => {
      axios.get (REACT_APP_API_URL +'/api/search/profile/'+id)
      .then(res=> {
      setPosts(res.data);
      
      })
      .catch(err =>{console.log(err)})
    
    },[])}
    return (
      DataFetching(),
      <React.Fragment>
        <Grid 
        alignItems="center"
        item xs={12}  >
        <Box className = {classes.paper} m={2}>
            <Typography variant="h5" color="Primary" component="p" >
                INFORMATIONS PERSONNELLES
                
            </Typography>
        </Box> 
         
        <Box className = {classes.paper} m={2}>
            <Dividers></Dividers>
        </Box> 
        <Box className = {classes.paper} m={2}>
            <Typography variant="h5" color="Primary" component="p" >
                DESCRIPTION
                
            </Typography>
        </Box>  

        <Box className = {classes.paper} m={2}>
            <Typography variant="h6"  component="p" >
            {posts.description}
            </Typography>
        </Box>
        </Grid>
      </React.Fragment>
    );
  }
  return (
    <div className={classes.root} >
      <Grid container spacing={1} >
        <Grid container item xs={12} alignItems="center"  >
        <Box  fontWeight="fontWeightLight" mx="auto" >
          <Typography variant="h2"   textAlign='center'  >
            Profil
          </Typography> 
        </Box>
        </Grid>
        <Grid container item xs={12} sm ={12} spacing={2} > 
          <FormRow />
        </Grid>
      </Grid>
    </div>
  );
}