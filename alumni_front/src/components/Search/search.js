import React from 'react';
import { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { useHistory } from 'react-router-dom';

import  Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box' 
import axios from "axios";
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from "@material-ui/core/InputAdornment"



import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import {REACT_APP_API_URL} from "../../http-common";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    
    marginTop: theme.spacing(15),
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
  blogsContainer: {
    paddingTop: theme.spacing(3)
  },
  blogTitle: {
    fontWeight: 800,
    paddingBottom: theme.spacing(3)
  },
  card: {
    maxWidth: "100%",
  },
  media: {
    height: 240
  },
  cardActions: {
    display: "flex",
    margin: "0 10px",
    justifyContent: "space-between"
  },
  author: {
    display: "flex"
  },
  paginationContainer: {
    display: "flex",
    justifyContent: "center"
  }
}));

const Search=(props) => {
  const [details,setdetails] =useState({description:'',domaine:'',date_naissance:'',pays:'', phone: '',statut: '', user:{}, image:''})
  const detailsarray = Array.from(details);
  const classes = useStyles();
  const [SearchTerm, setSearchTerm] = useState("");

  const handleInput = event => {
    setSearchTerm(event.target.value);
  };

  const history = useHistory();
  function profileRedirect(id) {
    history.push("/search_profil/"+ id);
  }

  function DataFetchingx(event) {
      event.preventDefault();
    
      axios.get (REACT_APP_API_URL +'/api/search/profile?search='+ SearchTerm)
      .then(res=> {
        setdetails(res.data);
      })
      .catch(err =>{console.log(err)})
}  
  function Infosfetching()    {
 
  const detailsinfos = detailsarray.map((info) => (
    
  <Grid item xs={12} sm={6} md={4}>
    
  <Card className={classes.card}>
    <CardActionArea onClick={()=>profileRedirect(info.user.id)}>
      <CardMedia
        className={classes.media}
        image="https://images.pexels.com/photos/2004161/pexels-photo-2004161.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
        title="Contemplative Reptile"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {info.domaine}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {info.description}
        </Typography>
      </CardContent>
    </CardActionArea>
    <CardActions className={classes.cardActions}>
      <Box className={classes.author}>
        <Avatar src={info.image} />
        <Box ml={2}>
          <Typography variant="subtitle2" component="p">
            {info.user.first_name} {info.user.last_name}
          </Typography>
          <Typography variant="subtitle2" color="textSecondary" component="p">
            
          </Typography>
        </Box>
      </Box>
      <Box>
        <BookmarkBorderIcon />
      </Box>
    </CardActions>
  </Card>
    
</Grid>
)) 
  return (

    <React.Fragment>
        
       {detailsinfos}
    </React.Fragment>

  )   ;
  }


  

 
  return (
    <div className={classes.root}>
        <Box  fontWeight="fontWeightLight" mx="auto" >
         <Typography variant="h4"  className={classes.blogTitle} >
          Rechercher une compétence
         </Typography> 

        </Box>
      <form noValidate onSubmit={DataFetchingx}>
      <TextField
        name="Search"
        label="Rechercher"
        value ={SearchTerm}
        onInput ={handleInput}
        InputProps={{
          endAdornment: (
            <InputAdornment position="start">
            <IconButton 
              aria-label="search" type="submit"
            >
              <SearchIcon />
            </IconButton>    
              
            </InputAdornment>
           )
          }}
      >

      </TextField>     

      </form>

      
    <Container maxWidth="lg" className={classes.blogsContainer}>
        
        <Grid container spacing={3}>
          
          <Infosfetching></Infosfetching>
                  
        </Grid>
        <Box my={4} className={classes.paginationContainer}>
         
        </Box>
      </Container>  
      
    </div>
  );
 
}



  
  

export default Search;