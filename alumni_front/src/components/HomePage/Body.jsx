import React, { Component } from 'react';
import Header from './header';
import About from './about';
import Services from './services';
import Testimonials from './testimonials';
import Team from './Team';
import Contact from './contact';
import JsonData from '../../data/data.json';


export class Body extends Component {
    state = {
        landingPageData: {},
      }
      getlandingPageData() {
        this.setState({landingPageData : JsonData})
      }
    
      componentDidMount() {
        this.getlandingPageData();
      }
    render() {
        return (
            <div>    
                <Header loggedInStatus={this.props.loggedInStatus} data={this.state.landingPageData.Header} />
                <About data={this.state.landingPageData.About} />
                <Team data={this.state.landingPageData.Team} />
                <Services data={this.state.landingPageData.Services} />
                <Testimonials data={this.state.landingPageData.Testimonials} />
                <Contact data={this.state.landingPageData.Contact} />
            </div>
        )
    }
}

export default Body
