import React, { Component } from "react";
import { Link } from 'react-router-dom';

export class Navigation extends Component {
  render() {
    let element =
    <React.Fragment>
      <li>
      <Link to="/login" className="page-scroll">
          Connexion
        </Link>
      </li>
      <li>
      <Link to="/signup" className="page-scroll">
          Inscription
      </Link>
      </li>
    </React.Fragment>
    if (localStorage.getItem("token")!== null) {
      element =
      <React.Fragment>
      <li>
      <Link to="/profil" className="page-scroll">
          Profil
        </Link>
      </li>
      <li>
      <Link onClick={this.props.handleLogout} to="/" className="page-scroll">
          Déconnexion
      </Link>
      </li>
      </React.Fragment>
    }

    return (
      <React.Fragment>
      <nav id="menu" className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              data-target="#bs-example-navbar-collapse-1">
              {" "}
              <span className="sr-only">Toggle navigation</span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
              <span className="icon-bar"></span>{" "}
            </button>
            <a className="navbar-brand page-scroll" href="/#page-top">
              TOGOALUMNI
            </a>{" "}
          </div>
          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="/#services" className="page-scroll">
                  Services
                </a>
              </li>
              <li>
                <a href="/#testimonials" className="page-scroll">
                  Témoignages
                </a>
              </li>
              <li>
                <a href="/#about" className="page-scroll">
                  Propos
                </a>
              </li>
              <li>
                <a href="/#contact" className="page-scroll">
                  contact
                </a>
              </li>
              {element}
            </ul>
          </div>
        </div>
      </nav>
      </React.Fragment>
    );
  }
}

export default Navigation;
