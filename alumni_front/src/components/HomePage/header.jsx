import React, { Component } from "react";

export class Header extends Component {

  

  render() {




    

    let element =
    <React.Fragment>
      <a
        href="/search"
        className="btn btn-custom btn-lg page-scroll">
        Rechercher
      </a>
    </React.Fragment>;
    if (localStorage.getItem("token")== null) {
      element =
      <React.Fragment>
        <a
          href="/login"
          className="btn btn-custom btn-lg page-scroll">
          Connexion
        </a>
        <a
          href="/search"
          className="btn btn-custom btn-lg page-scroll">
          Rechercher
        </a>
        <a
          href="/signup"
          className="btn btn-custom btn-lg page-scroll">
          Inscription
        </a>
  </React.Fragment>;}
    return (
      <header id="header">
        <div className="intro">
          <div className="overlay">
            <div className="container">
              <div className="row">
                <div className="col-md-8 col-md-offset-2 intro-text">
                  <h1>
                    {this.props.data ? this.props.data.title : "Loading"}
                    <span></span>
                  </h1>
                  <p>
                    {this.props.data ? this.props.data.paragraph : "Loading"}
                  </p>
                  {element}
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
