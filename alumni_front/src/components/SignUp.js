import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {useForm} from 'react-hook-form';
import ErrorIcon from '@material-ui/icons/Error';
import { useDispatch } from "react-redux";
import { setSnackbar } from "./redux/ducks/snackbar";
import { useHistory } from 'react-router-dom';
import {REACT_APP_API_URL} from "../http-common";


function printErrors(err) {
  let tmp = "";
  for (var key of Object.keys(err)) {
    tmp = tmp + "  | " + err[key] + " | ";
  }
  return tmp;
}

function mapErrors(error_description) {
  const USERNAME_EXIST = "user with this username already exists.";
  const EMAIL_EXIST = "user with this email already exists.";
  const USERNAME_EXIST_MSG = "Un utilisateur avec ce nom d'utilisateur existe déjà";
  const EMAIL_EXIST_MSG = "Un utilisateur avec cette adresse email existe déjà";
  const ERROR_GENERIC_MSG = "Une erreur est survenue";
  let error_msg = "";
  console.log("debug-mapErrors");
  console.log(error_description);
  switch (error_description) {
    case USERNAME_EXIST:
      error_msg = USERNAME_EXIST_MSG;
      break;
    case EMAIL_EXIST:
      error_msg = EMAIL_EXIST_MSG;
      break;
    default:
      error_msg = ERROR_GENERIC_MSG;
  }
  return error_msg;
}

function handleErrors(error_object) {
  //recieve json error object, map and print the errors

}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(12),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
    
  },
  
    warningtext : {
      fontSize: "1em",
      color: "#de453c",  
    },
    WarningIconmargin : {
      marginBottom: "-2px",
      fontSize: "small"
    }
}));

export default function SignUp() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const history = useHistory();
  // const dispatch = useDispatch();

  const routeChange = () =>{ 
    let path = "/"; 
    history.push(path);
  }

  const  {register,handleSubmit,errors, watch }=useForm({
      mode: 'onSubmit',
      reValidateMode: 'onChange',
      defaultValues: {},
      resolver: undefined,
      context: undefined,
      criteriaMode: "firstError",
      shouldFocusError: true,
      shouldUnregister: true,
    });
  const onSubmit =data =>{
    console.log(data);
    fetch(REACT_APP_API_URL +'/api/membership/signup/',{
      method:'POST',
      headers:{'Content-Type':'application/json'},
      body:JSON.stringify(data)
    })
    //.then(response => response.json())
    .then(response => Promise.all([response.ok, response.json()]))
    .then(([response, body]) => {
      if (!response) {
        console.log("debug-status");
        console.log(response);
        console.log(body);
        console.log("debug-body");
        console.log(body); 
        console.log(Object.values(body)); 
        console.log(Object.keys(body)); 
        let errors_desc_array = Object.keys(body)
        .map(function(key) {
          return body[key][0];
        });
        console.log(errors_desc_array); 
        for (var i = 0; i < errors_desc_array.length; i++) {
          errors_desc_array[i] = mapErrors(errors_desc_array[i]);
           console.log(errors_desc_array[i]);
        }
        // console.log(errors_desc_array); 
        throw Error(errors_desc_array.join(' - '));
        // console.log(Object.keys(response));
        // console.log(response.email);
        /* let tmp = printErrors(response);
        dispatch(
          setSnackbar(
            true,
            "error",
            tmp,
            "6000"
          )); */
        // throw Error(response);
      }
      return response;
    })
    .then(
      data=>{
        console.log(data);  
        routeChange();
        dispatch(
          setSnackbar(
            true,
            "success",
            "Inscription réussie. Veuillez confirmer votre inscription par le lien envoyé par email!",
            "10000"
          )
        )
      }
    ).catch(function(error) {
      // Faire une fonction pour afficher les erreurs
      console.log("debug-error");
      console.log(error.name);
      console.log(error.message);
      // let tmp = printErrors(error.message);
      dispatch(
        setSnackbar(
          true,
          "error",
          error.message,
          "6000"
        ));
    })
  }
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}> 
        </Avatar>
        <Typography component="h1" variant="h4">
          S'inscrire
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={2}>
          <Grid item xs={12} >
              <TextField
                autoComplete="username"
                name="username"
                variant="outlined"
                required
                fullWidth
                id="username"
                label="Nom d'utilisateur"
                inputRef={register({required: true, minLength: 2})}
                autoFocus
              />
          {errors.username &&errors.username.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
          {errors.username &&errors.username.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}
          
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="first_name"
                variant="outlined"
                required
                inputRef={register({required: true, minLength: 2})}
                fullWidth
                id="firstName"
                label="Prénom"
                autoFocus/>

            {errors.first_name &&errors.first_name.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
            {errors.first_name &&errors.first_name.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}
          
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                inputRef={register({required: true, minLength: 2})}
                id="lastName"
                label="Nom"
                name="last_name"
                autoComplete="lname"
              />
            {errors.last_name &&errors.last_name.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
            {errors.last_name &&errors.last_name.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}
          
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Adresse email"        
                name="email"
                inputRef={register ({required:true, pattern:{ value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
                }})}
                autoComplete="email"
              />
              {errors.email &&errors.email.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
              {errors.email &&errors.email.type === "pattern" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Saisissez une adresse email</p>)}
          
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                inputRef={register({required: true, minLength: 8})} 
                name="password1"
                label="Password"
                type="password"
                id="password1"
                autoComplete="current-password"
              />
            {errors.password1 &&errors.password1.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
            {errors.password1 &&errors.password1.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}
          
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                inputRef={register({
                  required:true,
                  validate: (value) => value === watch('password1')})}
                
                name="password2"
                label="Confirmer Password"
                type="password"
                id="password2"
                autoComplete="current-password"
              />
            {errors.password2 &&errors.password2.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
            {errors.password2 &&errors.password2.type === "validate" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Les deux champs ne sont pas identiques</p>)}
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="J'accepte de recevoir des informations et notfications à travers mon email"
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Inscription
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/login" variant="body2" className={classes.textsize}>
                Vous avez déjà un compte? Se connecter
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      
    </Container>
  );
}