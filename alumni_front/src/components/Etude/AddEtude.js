import React, {useState} from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from '@material-ui/core/styles';
import EtudeDataService from "../../services/UserProfileservice";
import {useDispatch} from "react-redux";
import {setSnackbar} from "../redux/ducks/snackbar";
import {useHistory} from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
    root: {

        //fontSize: '1.5em',
        marginTop: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(),
        },
    },
    button: {},
    button2: {},
    textMargin: {
        marginTop: theme.spacing(13)
    },
    textMargin1: {
        marginTop: theme.spacing(20)
    },
    textWidth: {
        fontSize: '18px',
        width: '70%',

    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        width: '80%',

        margin: 10,
        fontSize: 50 //??? Doesnt work
    },
    resize: {
        fontSize: 14
    },
    labelmarg: {
        marginBottom: theme.spacing(11),
    },

}))

function AddEtude() {
    const classes = useStyles();
    const dispatch = useDispatch();
    const history = useHistory();

    const routeChange = () => {
        let path = "/profil";
        history.push(path);
    }

    const initialEtudeState = {
        user_profile: localStorage.getItem('id'),
        current_studies: "",
        diplome: "",
        university: "",
        from_date: "",
        to_date: null,
        description: ""
    };
    const [Etude, setEtude] = useState(initialEtudeState);
    const [submitted, setSubmitted] = useState(false);

    const handleInputChange = event => {
        const {name, value} = event.target;
        setEtude({...Etude, [name]: value});
    };

    const saveEtude = () => {
        var data = {
            user_profile: Etude.user_profile,
            current_studies: Etude.current_studies,
            diplome: Etude.diplome,
            university: Etude.university,
            from_date: Etude.from_date,
            to_date: Etude.to_date,
            description: Etude.description
        };

        EtudeDataService.createEtude(data)
            .then(response => {
                setEtude({
                    user_profile: localStorage.getItem('id'),
                    current_studies: response.data.current_studies,
                    diplome: response.data.diplome,
                    from_date: response.data.from_date,
                    to_date: response.data.to_date,
                    university: response.data.university,
                    description: response.data.description,
                    published: response.data.published
                });
                setSubmitted(true);
                dispatch(
                    setSnackbar(
                        true,
                        "success",
                        "Étude ajouté avec succès!",
                        4000
                    )
                )
                routeChange();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const newEtude = () => {
        setEtude(initialEtudeState);
        setSubmitted(false);
    };

    return (
        <Container>
            <h2 className={classes.textMargin}>Ajouter une étude</h2>


            <form className={classes.root} onSubmit={saveEtude}>


                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        id="diplome"
                        name="diplome"
                        label={<span style={{fontSize: '1.2em'}}>{"Intitulé de la formation"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={Etude.diplome}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        id="university"
                        name="university"
                        label={<span style={{fontSize: '1.2em'}}>{"Ecole ou Université"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={Etude.university}
                        onChange={handleInputChange}
                    />
                </Grid>

                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        type="date"
                        id="from_date"
                        name="from_date"
                        label={<span style={{fontSize: '1.2em'}}>{"Date de début"}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        value={Etude.from_date}
                        onChange={handleInputChange}
                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        variant="outlined"
                        type="date"
                        id="to_date"
                        name="to_date"
                        label={<span style={{fontSize: '1.2em'}}>{"Date de fin"}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        value={Etude.to_date}
                        onChange={handleInputChange}
                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        multiline
                        id="description"
                        name="description"
                        variant="outlined"
                        label={<span style={{fontSize: '1.2em'}}>{"Description"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={Etude.description}
                        onChange={handleInputChange}
                    />
                </Grid>

                <Grid item xs={12}>


                </Grid>
                <br/><br/>
                <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    type="submit"


                >Ajouter</Button>
            </form>
        </Container>
    );
}

export default AddEtude;