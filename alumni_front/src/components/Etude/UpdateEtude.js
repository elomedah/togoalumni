import React, {useState, useEffect} from "react";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import DataService from "../../services/UserProfileservice";
import {useDispatch} from "react-redux";
import {setSnackbar} from "../redux/ducks/snackbar";
import {useHistory} from "react-router-dom";
import Link from "@material-ui/core/Link";
import {useForm} from "react-hook-form";
import FormData from "form-data";

const useStyles = makeStyles((theme) => ({
    root: {

        //fontSize: '1.5em',
        marginTop: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(),
        },
    },
    button: {
        marginLeft: '15%',
        marginRight: 'auto',

    },
    button2: {
        marginLeft: '5%',
        marginRight: 'auto',
    },
    textMargin: {
        marginTop: theme.spacing(13)
    },
    textMargin1: {
        marginTop: theme.spacing(20)
    },
    textWidth: {
        fontSize: '18px',
        width: '70%',

    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        width: '80%',

        margin: 10,
        fontSize: 50 //??? Doesnt work
    },
    resize: {
        fontSize: 14
    },
    labelmarg: {
        marginBottom: theme.spacing(11),
    },
    InputDescription: {
        ontSize: 14,
        height: 80,
    }

}))


function UpdateEtude(props) {
    const {register, handleSubmit} = useForm();
    const history = useHistory();
    const dispatch = useDispatch();

    const initialEtudeState = {
        user_profile: localStorage.getItem('id'),
        current_studies: "",
        diplome: "",
        university: "",
        from_date: "",
        to_date: null,
        description: ""
    };
    const [etude, setEtude] = useState(initialEtudeState);

    const etudeId = props.match.params.id;

    const handleInputChange = e => {
        const {name, value} = e.target;
        setEtude({...etude, [name]: value})
    };
    useEffect(() => {

        DataService.getEtude(etudeId).then(study => {
            if (study.data) {
                setEtude(study.data);
            }
        })
            .catch(error => {
                dispatch(
                    setSnackbar(
                        true,
                        "error",
                        "L'élement demandé est introuvable",
                        "4000"
                    )
                )
            });
    }, []);

    const routeChange = () => {
        let path = "/profil";
        history.push(path);
    };

    let datax = new FormData();
    const onSubmit = (data) => {
        const updatedData = {
            diplome: data.diplome,
            university: data.university,
            from_date: data.from_date,
            to_date: data.to_date,
            description: data.description
        };

        DataService.updateEtude(etudeId, updatedData).then(json => {
            dispatch(
                setSnackbar(
                    true,
                    "success",
                    "Etude mise à jour avec succès!",
                    "4000"
                )
            );
            routeChange();
        })
            .catch(error => {
                dispatch(
                    setSnackbar(
                        true,
                        "error",
                        "Echéc de mise à jour",
                        "4000"
                    )
                )
            });
    };


    const classes = useStyles();

    return (
        <Container>
            <h2 className={classes.textMargin}>Mettre à jour une année d'étude</h2>


            <form onSubmit={handleSubmit(onSubmit)}>


                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        id="diplome"
                        name="diplome"
                        label={<span style={{fontSize: '1.2em'}}>{"Intitulé de la formation"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        inputRef={register}
                        value={etude.diplome}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        id="university"
                        name="university"
                        label={<span style={{fontSize: '1.2em'}}>{"Ecole ou Université "}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={etude.university}
                        inputRef={register}
                        onChange={handleInputChange}
                    />
                </Grid>

                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        variant="outlined"
                        type="date"
                        id="from_date"
                        name="from_date"
                        label={<span style={{fontSize: '1.2em'}}>{"Date de début"}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        value={etude.from_date}
                        onChange={handleInputChange}
                        inputRef={register}
                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        variant="outlined"
                        type="date"
                        id="to_date"
                        name="to_date"
                        label={<span style={{fontSize: '1.2em'}}>{"Date de fin"}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        value={etude.to_date}
                        onChange={handleInputChange}
                        inputRef={register}
                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        multiline
                        id="description"
                        name="description"
                        variant="outlined"
                        label={<span style={{fontSize: '1.2em'}}>{"Description"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={etude.description}
                        inputRef={register}
                        onChange={handleInputChange}
                    />
                </Grid>

                <Grid item xs={12}>
                    <Button
                        className={classes.button2}
                        variant="contained"
                        color="default"
                    >
                        <Link href="/profil" underline="none">
                            Annuler
                        </Link>
                    </Button>

                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        type="submit"
                    >Enregistrer
                    </Button>
                </Grid>
            </form>
        </Container>
    );
}

export default UpdateEtude;