import React, { useState } from "react";
import { setSnackbar } from "./redux/ducks/snackbar";
import { REACT_APP_API_URL } from "../http-common";
import { useDispatch } from "react-redux";
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";

function Validation() {

    const history = useHistory();
    const dispatch = useDispatch();
    const token = new URLSearchParams(window.location.search).get('token');
    const [timer, setTimer] = useState(5);
    const [isValid, setValid] = useState();

    if (token) {
        fetch(REACT_APP_API_URL + `/api/membership/email-verify?token=${token}`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        }).then(response => {
            if (!response.ok) {
                setValid(false);
                dispatch(
                    setSnackbar(
                        true,
                        "error",
                        "response",
                        "6000"
                    ));
                throw Error(response);
            }
            return response.json()
        })
            .then(() => {
                setValid(true);
                dispatch(
                    setSnackbar(
                        true,
                        "success",
                        "Token validé!",
                        "4000"
                    )
                )

                setTimeout(() => {
                    history.push("/");
                }, timer * 1000)

                setInterval(() => {
                    setTimer(timer - 1)
                }, 1000)
            }
            ).catch(error => {
                setValid(false);
                console.error(error);
            })
    }

    return (
        <div>
            {token ?
                <div>
                    {isValid === undefined && <span>Veuillez patienter, le compte est en train d'être vérifié</span>}
                    {isValid === false && <span>Le compte n'est pas valide</span>}
                    {isValid && <span>Le token est valide. Vous serez redirigé a la page d'accueil dans {timer} <Link to="/">principale</Link> </span>}
                </div> :
                <div>Veuillez vérifier que l'url a été bien saisie</div>
            }
        </div>
    );
}

export default Validation;