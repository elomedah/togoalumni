import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { useHistory } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';  
import Container from '@material-ui/core/Container';
import {useForm} from 'react-hook-form';
import ErrorIcon from '@material-ui/icons/Error';
import { useDispatch } from "react-redux";
import { setSnackbar } from "./redux/ducks/snackbar";
import {REACT_APP_API_URL} from "../http-common";


function printErrors(err) {
  let tmp = "";
  for (var key of Object.keys(err)) {
    tmp = tmp + "  | " + err[key] + " | ";
  }
  return tmp;
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(12),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    
    
  },
  warningtext : {
    fontSize: "1em",
    color: "#de453c",
    
    
  },
  WarningIconmargin : {
    marginBottom: "-2px",
    fontSize: "small"
  }
}));

const SignIn =(props) => {
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const routeChange = () =>{ 
    let path = "/profil"; 
    history.push(path);
  }
  const  {register,handleSubmit,errors }=useForm(
    {
  mode: 'onSubmit',
  reValidateMode: 'onChange',
  defaultValues: {},
  resolver: undefined,
  context: undefined,
  criteriaMode: "firstError",
  shouldFocusError: true,
  shouldUnregister: true,
    }
  );
  const onSubmit =data =>{
    fetch(REACT_APP_API_URL +'/api/mem;;;;;;;h_hbership/login/',{
      method:'POST',
      headers:{'Content-Type':'application/json'},
      body:JSON.stringify(data)
    }).then(response => {
        if (!response.ok) {
            let tmp = printErrors(response);
            dispatch(
                setSnackbar(
                    true,
                    "error",
                    tmp,
                    "6000"
                ));
            throw Error(response);
        }
      return response.json()
    })
      .then(data => {  
          localStorage.setItem('token',data.tokens.access); 
          routeChange();
          props.handleLogin(data);
          dispatch(
            setSnackbar(
              true,
              "success",
              "Vous êtes connecté!",
              "4000"
            )
          )
        
      }
    ).catch(error => {
      console.error(error);
      // dispatch(
      //   setSnackbar(
      //     true,
      //     "error",
      //     "Connextion échoué!",
      //     "4000"
      //   )
      // )
    })
  }
  return (
    
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
        </Avatar> 
        <Typography component="h1" variant="h4">
          Connectez-vous
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(onSubmit)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            inputRef={register ({required:true, minLength:8, pattern:{ value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i
            }})}
            label="Email"
            name="email"
            autoComplete="email"
            autoFocus
          />
          {errors.email &&errors.email.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
          {errors.email &&errors.email.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}
          {errors.email &&errors.email.type === "pattern" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Saisissez une adresse email</p>)}
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Mot de passe"
            type="password"
            id="password"
            inputRef={register({required: true, minLength: 4})}
            autoComplete="current-password"
          />
          {errors.password &&errors.password.type === "required" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est obligatoire</p>)}
          {errors.password &&errors.password.type === "minLength" &&(<p className={classes.warningtext}><ErrorIcon className={classes.WarningIconmargin}/> Ce champ est trop court</p>)}

          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label={<span>{'Se souvenir de moi'}</span>}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            
            >
            Connexion
          </Button>
          <Grid container >
            <Grid item xs>
              <Link href="/ForgetPassword" variant="body2" >
                Mot de passe oublié?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/signup" variant="body2" >
                Pas de compte? S'inscrire
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
    
  );
}
export default SignIn;