import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import DataService from "../../services/UserProfileservice";
import { useDispatch } from "react-redux";
import { setSnackbar } from "../redux/ducks/snackbar";
import { useHistory } from 'react-router-dom';
import Link from '@material-ui/core/Link';
import { useForm } from "react-hook-form";
import FormData from 'form-data';
import axios from 'axios'
import { REACT_APP_API_URL } from "../../http-common";
import { useEffect } from 'react';

const useStyles = makeStyles((theme) => ({
  root: {

    //fontSize: '1.5em',
    marginTop: theme.spacing(4),
    '& .MuiTextField-root': {
      margin: theme.spacing(),
    },
  },
  button: {
    marginLeft: '15%',
    marginRight: 'auto',

  },
  button2: {
    marginLeft: '5%',
    marginRight: 'auto',
  },
  textMargin: {
    marginTop: theme.spacing(13)
  },
  textMargin1: {
    marginTop: theme.spacing(20)
  },
  textWidth: {
    fontSize: '18px',
    width: '70%',

  },
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    width: '80%',

    margin: 10,
    fontSize: 50 //??? Doesnt work
  },
  resize: {
    fontSize: 14
  },
  labelmarg: {
    marginBottom: theme.spacing(11),
  },
  InputDescription: {
    ontSize: 14,
    height: 80,
  }

}))



function Updateprofil() {
  const { register, handleSubmit } = useForm();
  const history = useHistory();
  const dispatch = useDispatch();

  const [resume, setResume] = useState({});

  const handleInputChange = e => {
    const { name, value } = e.target;
    setResume({ ...resume, [name]: value })
  };

  useEffect(() => {
    axios.get(REACT_APP_API_URL + '/api/membership/profile/',
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => {
        setResume(res.data);
      })
      .catch(err => { console.log(err) })

  }, [])


  const routeChange = () => {
    let path = "/profil";
    history.push(path);
  }

  let datax = new FormData();
  const onSubmit = (data) => {
    if (data && data.image && data.image.length) {
      datax.append('image', data.image[0], data.image[0].name)
    }
    datax.append('description', data.description)
    datax.append('statut', data.statut)
    datax.append('date_naissance', data.date_naissance)
    datax.append('domaine', data.domaine)
    datax.append('phone', data.phone)
    datax.append('pays', data.pays)
    DataService.updateProfile(localStorage.getItem('id'), datax).then(json => {
      dispatch(
        setSnackbar(
          true,
          "success",
          "Profil mis à jour avec succès!",
          "4000"
        )
      )
      history.push('/profil');
    })
      .catch(error => {
        dispatch(
          setSnackbar(
            true,
            "error",
            "Echéc de mise à jour du profil",
            "4000"
          )
        )
      });
  }


  const classes = useStyles()

  return (

    <Container>
      <h2 className={classes.textMargin}>Modifier les informations</h2>


      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}  >

        <Grid item xs={12} md={6}>
          <TextField
            id="pays"
            name="pays"
            value={resume.pays}
            variant="outlined"
            label={<span style={{ fontSize: '1.2em' }}>{"Pays"}</span>}
            InputProps={{
              classes: {
                input: classes.resize,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            inputRef={register}
            onChange={handleInputChange}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            variant="outlined"
            id="phone"
            name="phone"
            value={resume.phone}
            onChange={handleInputChange}
            label={<span style={{ fontSize: '1.2em' }}>{"Numéro de télephone"}</span>}
            InputProps={{
              classes: {
                input: classes.resize,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            inputRef={register}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            variant="outlined"
            id="statut"
            name="statut"
            value={resume.statut}
            onChange={handleInputChange}
            label={<span style={{ fontSize: '1.2em' }}>{"Statut"}</span>}
            InputProps={{
              classes: {
                input: classes.resize,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            inputRef={register}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            variant="outlined"
            id="domaine"
            name="domaine"
            value={resume.domaine}
            onChange={handleInputChange}
            label={<span style={{ fontSize: '1.2em' }}>{"Domaine"}</span>}
            InputProps={{
              classes: {
                input: classes.resize,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            inputRef={register}
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            variant="outlined"
            type="date"
            id="date_naissance"
            value={resume.date_naissance}
            onChange={handleInputChange}
            name="date_naissance"
            label={<span style={{ fontSize: '1.2em' }}>{"Date de naissance"}</span>}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            inputRef={register}
            fullWidth />
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            multiline
            id="description"
            variant="outlined"
            name="description"
            value={resume.description}
            onChange={handleInputChange}
            label={<span style={{ fontSize: '1.2em' }}>{"Description"}</span>}
            InputProps={{
              classes: {
                input: classes.InputDescription,
              },
            }}
            InputLabelProps={{
              shrink: true,
            }}
            className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            inputRef={register}
          />
        </Grid>

        <Grid item xs={12}>
          <h3>Veuillez choisir une photo de profil</h3>
          <input
            ref={register}
            type="file"
            name="image"
          />
        </Grid>

        <br /><br /><br />

        <Grid item xs={12}>
          <Button
            className={classes.button2}
            variant="contained"
            color="gray"
          >
            <Link href="/profil" underline="none" >
              Annuler
              </Link>
          </Button>

          <Button
            className={classes.button}
            variant="contained"
            color="primary"
            type="submit"
          >Enregistrer
        </Button>
        </Grid>

      </form>
    </Container>
  );
}

export default Updateprofil;