import React from "react";
import "./index.css";
import {Grid,Box ,Typography} from '@material-ui/core'
import Fade from 'react-reveal/Fade';
import {useState, useEffect} from 'react';
import axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import { makeStyles } from '@material-ui/core/styles';
import {REACT_APP_API_URL} from "../../../http-common";
const useStyles = makeStyles((theme)=>({
    avatar: {
      width:"200px",
      height: "200px",
      marginTop: "30px",
      marginLeft: "auto",
      marginRight: "auto"

    },
    
    
  }));

export default function  BackgroundImagePage() {
  const classes = useStyles();
  const [Picture,setPicture] =useState({image:''})
  const detailsarray = Array.from(Picture);
    function ImageFetching() {
    
    useEffect(() => {
      axios.get (REACT_APP_API_URL +'/api/search/profile/'+ localStorage.getItem('id'),
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }})
      .then(res=> {
      setPicture(res.data);
      
      })
      .catch(err =>{console.log(err)})
    
    },[])}
  return (
  ImageFetching(),
  <div className="bg" >
     <Fade right>
      <div  className ="centered">
        
      <Grid  
            container
            direction="column"
            justify="flex-end"
            alignItems="center"
          >
          <Grid >
        
            <Box >      
            <Avatar className={classes.avatar} src={Picture.image} />        
            </Box>
               
           </Grid>
        </Grid>
       
            </div> 
    </Fade>
  </div>
     )
};
