import React, { Component } from 'react'

import AboutMe from './AboutMe';
import Resume from './Resume';
import Zoom from 'react-reveal/Zoom'
import BackgroundImagePage from './begins/begins'

export class Profil extends Component {
  

  render() {
    return (
      <div>
        <BackgroundImagePage></BackgroundImagePage>
        <Zoom>
        <AboutMe></AboutMe>
        <Resume></Resume>
        </Zoom>
      </div>
    )
  }
}

export default Profil
