
import React, {useState, useEffect} from "react";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";
import DataService from "../../services/UserProfileservice";
import {useDispatch} from "react-redux";
import {setSnackbar} from "../redux/ducks/snackbar";
import {useHistory} from "react-router-dom";
import Link from "@material-ui/core/Link";
import {useForm} from "react-hook-form";
import FormData from "form-data";

const useStyles = makeStyles((theme) => ({
    root: {

        //fontSize: '1.5em',
        marginTop: theme.spacing(4),
        '& .MuiTextField-root': {
            margin: theme.spacing(),
        },
    },
    button: {
        marginLeft: '15%',
        marginRight: 'auto',

    },
    button2: {
        marginLeft: '5%',
        marginRight: 'auto',
    },
    textMargin: {
        marginTop: theme.spacing(13)
    },
    textMargin1: {
        marginTop: theme.spacing(20)
    },
    textWidth: {
        fontSize: '18px',
        width: '70%',

    },
    textField: {
        // marginLeft: theme.spacing.unit,
        // marginRight: theme.spacing.unit,
        width: '80%',

        margin: 10,
        fontSize: 50 //??? Doesnt work
    },
    resize: {
        fontSize: 14
    },
    labelmarg: {
        marginBottom: theme.spacing(11),
    },
    InputDescription: {
        ontSize: 14,
        height: 80,
    }

}))


function UpdateExperience(props) {
    const {register, handleSubmit} = useForm();
    const history = useHistory();
    const dispatch = useDispatch();

    const initialExperience = {
        user_profile: "",
        current_studies: "",
        job_position: "",
        place: "",
        from_date: "",
        to_date: "",
        description: ""
    };
    const [experience, setExperience] = useState(initialExperience);

    const experienceId = props.match.params.id;

    const handleInputChange = e => {
        const {name, value} = e.target;
        setExperience({...experience, [name]: value})
    };
    useEffect(() => {

        DataService.getExperience(experienceId).then(experience => {
            if (experience.data) {
                setExperience(experience.data);
            }
        })
            .catch(error => {
                dispatch(
                    setSnackbar(
                        true,
                        "error",
                        "L'élement demandé est introuvable",
                        "4000"
                    )
                )
            });
    }, []);

    const routeChange = () => {
        let path = "/profil";
        history.push(path);
    };

    const onSubmit = (data) => {
        const updatedData = {
            user_profile: localStorage.getItem('id'),
            job_position: data.job_position,
            place: data.place,
            from_date: data.from_date,
            to_date: data.to_date,
            description: data.description
        };

        DataService.updateExperience(experienceId, updatedData).then(json => {
            dispatch(
                setSnackbar(
                    true,
                    "success",
                    "Experience mise à jour avec succès!",
                    "4000"
                )
            );
        })
            .catch(error => {
                dispatch(
                    setSnackbar(
                        true,
                        "error",
                        "Echéc de mise à jour",
                        "4000"
                    )
                )
            });
    };


    const classes = useStyles();

    return (
        <Container>
            <h2 className={classes.textMargin}>Mettre à jour une expérience</h2>


            <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="job_position"
                        name="job_position"
                        label={<span style={{ fontSize: '1.2em' }}>{"POSTE OCCUPÉ"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        inputRef={register}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={experience.job_position}
                        onChange={handleInputChange}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="place"
                        name="place"
                        label={<span style={{ fontSize: '1.2em' }}>{"Lieu de travail"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        inputRef={register}

                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={experience.place}
                        onChange={handleInputChange}
                    />
                </Grid>

                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        type="date"
                        id="from_date"
                        name="from_date"
                        label={<span style={{ fontSize: '1.2em' }}>{"Date de début"}</span>}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        inputRef={register}

                        className={classes.textField}
                        value={experience.from_date}
                        onChange={handleInputChange}
                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField

                        type="date"
                        id="to_date"
                        name="to_date"
                        label={<span style={{ fontSize: '1.2em' }}>{"Date de fin"}</span>}
                        className={classes.textField}
                        value={experience.to_date}
                        onChange={handleInputChange}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        inputRef={register}

                        fullWidth/>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField

                        id="description"
                        name="description"
                        label={<span style={{ fontSize: '1.2em' }}>{"Description"}</span>}
                        InputProps={{
                            classes: {
                                input: classes.resize,
                            },
                        }}
                        className={classes.textField}
                        fullWidth
                        autoComplete="shipping address-line1"
                        value={experience.description}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        inputRef={register}
                        onChange={handleInputChange}/>
                </Grid>

                <Grid item xs={12} md={6}>
                </Grid>
                <Grid item xs={12} md={6}>
                    <Button
                        className={classes.button2}
                        variant="contained"
                        color="default"
                    >
                        <Link href="/profil" underline="none">
                            Annuler
                        </Link>
                    </Button>

                    <Button
                        className={classes.button}
                        variant="contained"
                        color="primary"
                        type="submit"
                    >Enregistrer
                    </Button>
                </Grid>
            </form>
        </Container>
    );
}

export default UpdateExperience;