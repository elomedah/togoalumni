import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import ExperienceDataService from "../../services/UserProfileservice";
import { useDispatch } from "react-redux";
import { setSnackbar } from "../redux/ducks/snackbar";
import { useHistory } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
  root: {
      
    //fontSize: '1.5em',
    marginTop: theme.spacing(4),
    '& .MuiTextField-root': {
      margin: theme.spacing(),
    },
  },
  button: {
    
    
  },
  button2: {
    
    
  },
  textMargin:{
    marginTop: theme.spacing(13)
  },
  textMargin1:{
    marginTop: theme.spacing(20)
  },
  textWidth:{
    fontSize: '18px',
    width: '70%',

  },
  textField: {
    // marginLeft: theme.spacing.unit,
    // marginRight: theme.spacing.unit,
    width: '80%',
    
    margin: 10,
    fontSize: 50 //??? Doesnt work
},
resize:{
    fontSize:14
  },
  labelmarg:{
    marginBottom :theme.spacing(11),
  },

}))

function AddExperience() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const routeChange = () =>{ 
    let path = "/profil"; 
    history.push(path);
  }

  const initialExperienceState = {
    user_profile: localStorage.getItem('id'),
    job_position: "",
    place: "",
    from_date: "",
    to_date: null,
    description: ""
  };
  const [Experience, setExperience] = useState(initialExperienceState);
  const [submitted, setSubmitted] = useState(false);

  const handleInputChange = event => {
    const { name, value } = event.target;
    setExperience({ ...Experience, [name]: value });
  };

  const saveExperience = () => {
    var data = {
      user_profile: Experience.user_profile,
      current_studies: Experience.current_studies,
      job_position: Experience.job_position,
      place: Experience.place,
      from_date: Experience.from_date,
      to_date: Experience.to_date,
      description: Experience.description
    };

    ExperienceDataService.createExperience(data)
      .then(response => {
        setExperience({
          id: response.data.id,
          job_position: response.data.job_position,
          from_date: response.data.from_date,
          to_date: response.data.to_date,
          place: response.data.place,
          description: response.data.description,
          published: response.data.published
        });
        setSubmitted(true);
        console.log(response.data);
        dispatch(
          setSnackbar(
            true,
            "success",
            "Experience professionnelle ajouté avec succès!",
            "4000"
          )
        )
        routeChange();
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <Container>
      <h2 className={classes.textMargin}>Ajouter une éxperience professionnelle</h2>
      <form className={classes.root} onSubmit={saveExperience}> 
        <Grid item xs={12}>
          <TextField
            required
            id="job_position"
            name="job_position"
            label={<span style={{ fontSize: '1.2em' }}>{"Poste occupé"}</span>}
            InputProps={{
                classes: {
                  input: classes.resize,
                },
              }}
              className={classes.textField}
            fullWidth
            autoComplete="shipping address-line1"
            value={Experience.job_position}
            InputLabelProps={{
                shrink: true,
            }}
            variant="outlined"
            onChange={handleInputChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="place"
            name="place"
            label={<span style={{ fontSize: '1.2em' }}>{"Lieu de travail"}</span>}
            InputProps={{
                classes: {
                  input: classes.resize,
                },
              }}
              className={classes.textField}
            InputLabelProps={{
                shrink: true,
            }}
            variant="outlined"
            fullWidth
            autoComplete="shipping address-line1"
            value={Experience.place}
            onChange={handleInputChange}
          />
        </Grid>
        
        <Grid item xs={12}>
          <TextField
            required
            type="date"
            id="from_date"
            name="from_date"
            label={<span style={{ fontSize: '1.2em' }}>{"Date de début"}</span>}
            InputLabelProps={{
              shrink: true,
            }}
              className={classes.textField}
              value={Experience.from_date}
              onChange={handleInputChange}
            variant="outlined"
            fullWidth/>
        </Grid>
        <Grid item xs={12}>
          <TextField
            
            type="date"
            id="to_date"
            name="to_date"
            label={<span style={{ fontSize: '1.2em' }}>{"Date de fin"}</span>}
            InputLabelProps={{
              shrink: true,
            }}
         variant="outlined"
              className={classes.textField}
              value={Experience.to_date}
              onChange={handleInputChange}
            fullWidth/>
        </Grid>
        <Grid item xs={12}>
          <TextField
            
            id="description"
            name="description"
            label={<span style={{ fontSize: '1.2em' }}>{"Description"}</span>}
            InputProps={{
                classes: {
                  input: classes.resize,
                },
              }}
              className={classes.textField}
            fullWidth
            InputLabelProps={{
                shrink: true,
            }}
            variant="outlined"
            autoComplete="shipping address-line1"
            value={Experience.description}
            onChange={handleInputChange}/>
          </Grid>
        
          <Grid item xs={12}>
          </Grid> 
        <br/><br/>
        <Button
          className={classes.button}
          variant="contained" 
          color="primary" 
          type="submit">
          Ajouter
        </Button>
      </form>
    </Container>
  );
}

export default AddExperience;