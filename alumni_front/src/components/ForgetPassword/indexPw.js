import React, {useContext, useState} from 'react'
import { Link } from 'react-router-dom'
import {REACT_APP_API_URL} from "../../http-common";

    const ForgetPassword =()=> {

        const base = useContext(REACT_APP_API_URL);
    
        const[email,setEmail]=useState("");

        


        const handleSubmit = e => {
                e.preventDefault();

                 
            

        }

        const disabled = email === "";
        return(                
                <div className="center-block">
                    <div className="">
                        <div className="formBoxLeftForget">
                        </div>
                        <div className="formBoxRight">
                            <div className="formContent">
                              
                            <h2>Mot de passe Oublié ?</h2>
                            <form className="form-inline" onSubmit={handleSubmit}>
                                <div className="form-group mb-2">
                                    <input onChange={e=> setEmail(e.target.value)} value={email} type="email" autoComplete/>
                                    <label for="staticEmail2" className="sr-only">Email</label>
                                </div>
                                <button disabled={disabled} >Récupéré</button>

                            </form>
                            <div className="linkContainer">
                                 <Link className="simpleLink" to="/login">Vous avez déjà un compte? Se connecter</Link>
                            </div>
                            </div>

                        </div>
                        </div>
                        </div>
                  
                

        )



    }

    export default ForgetPassword