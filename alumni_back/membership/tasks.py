from celery import shared_task
from celery.utils.log import get_task_logger
from django.core.mail import EmailMessage, EmailMultiAlternatives

@shared_task
def send_email_alternatives(data, email_html_message):
    subject = data["subject"]
    text_content = data["text_content"]
    from_email = data["from_email"]
    to = data["to"]
    msg = EmailMultiAlternatives(subject,text_content,from_email,to)
    msg.attach_alternative(email_html_message, "text/html")
    msg.send()
