from django.shortcuts import render
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.conf import settings
from django.dispatch import receiver
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.contrib.auth import login, authenticate
from django.contrib.auth import logout as django_logout

from rest_framework import generics
from rest_framework import permissions, status
from rest_framework.generics import GenericAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny

from django_rest_passwordreset.signals import reset_password_token_created
import jwt

from .models import User, UserProfile, Studies, ProfessionalCareer
from .serializers import *
from .permissions import IsOwnerProfileOrReadOnly
from .tasks import send_email_alternatives

class RegisterView(generics.GenericAPIView):

    serializer_class = UserSerializer

    def post(self, request):
        user = request.data
        serializer = self.serializer_class(data=user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user_data= serializer.data
        user= User.objects.get(email=user_data['email'])

        token=RefreshToken.for_user(user).access_token

        relativeLink= reverse('email-verify')
        #current_site=get_current_site(request).domain
        #host = request.get_host()
        #port = request.get_port()
        #backend_link=host+relativeLink+"?token="+str(token)
        host = request.META['HTTP_ORIGIN']
        frontend_link = host+"/#/validation?token="+str(token)+"/" # front uses hash technic

        context = {  ## for templating
            'user':user,
            'frontend_link':frontend_link,
            'token':token
        }

        # render email text
        email_html_message = render_to_string('email/user_verify_email.html', context)
        email_plaintext_message = render_to_string('email/user_verify_email.txt', context)

        data = {
            "subject": "Vérification d'email pour {title}".format(title="Togo World Alumni"),
            "text_content":email_plaintext_message,
            "from_email":settings.DEFAULT_FROM_EMAIL,
            "to":[user.email],
        }
        send_email_alternatives.delay(data,email_html_message) ## to send email in background

        return Response(user_data,status=status.HTTP_201_CREATED)
    

class VerifyEmail(generics.GenericAPIView):
    serializer_class = EmailVerificationSerializer
    def get(self,request):
        token= request.GET.get('token')
        token = token[:-1] ##to delete the last character that is '/'
        try:
            payload = jwt.decode(token, settings.SECRET_KEY)
            user= User.objects.get(id=payload['user_id'])
            if not user.is_verified:
                user.is_verified= True
                user.save()
            return Response({'email':'Your email is successfully activated'},status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as identifier:
            return Response({'error':'Activation link expired'},status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as identifier:
            return Response({'error':'Invalid token'}, status=status.HTTP_400_BAD_REQUEST)

class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return Response(serializer.data, status=status.HTTP_200_OK)

class LogoutAPIView(APIView): ### this is not yet functionnal
    permission_classes = (AllowAny,)
    def post(self, request):
        #user=request.user
        #token=RefreshToken.for_user(user).access_token
        print("Request data ", request.data)
        refresh_token = request.data["refresh"]
        token = RefreshToken(refresh_token)
        token.blacklist()

        return Response(status=status.HTTP_205_RESET_CONTENT)


class UserProfileAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk=None):
        header = JWTAuthentication.get_header(self, request)
        try:
            raw_token = JWTAuthentication.get_raw_token(self,header)
            valid_token = JWTAuthentication.get_validated_token(self, raw_token)
            user= JWTAuthentication.get_user(self,valid_token)
            if UserProfile.objects.get(user=user) == None:
                user_profile = UserProfile.objects.create(user=user)
            else:
                user_profile = UserProfile.objects.get(user__id=user.id)
            serializer = self.serializer_class(user_profile)
            status_code = status.HTTP_200_OK

            return Response(serializer.data,status=status.HTTP_200_OK)
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'success': 'false',
                'status code': status.HTTP_400_BAD_REQUEST,
                'error': str(e)
                }
        return Response(response, status=status_code)


class StudiesCreateAPIView(generics.CreateAPIView):
    serializer_class = StudiesSerializer
    queryset = Studies.objects.all()
    permission_classes = (IsAuthenticated, IsOwnerProfileOrReadOnly)

class StudiesUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = StudiesSerializer
    queryset = Studies.objects.all()
    permission_classes = (IsAuthenticated,)

class ProCareerCreateAPIView(generics.CreateAPIView):
    serializer_class = ProfessionalCareerSerializer
    queryset = ProfessionalCareer.objects.all()
    permission_classes = (IsAuthenticated, IsOwnerProfileOrReadOnly)

class ProCareerUpdateDeleteAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProfessionalCareerSerializer
    queryset = ProfessionalCareer.objects.all()
    permission_classes = (IsAuthenticated,)

class CustomPasswordResetView:
    @receiver(reset_password_token_created)
    def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
        """
        Handles password reset tokens
        When a token is created, an e-mail needs to be sent to the user
        :param sender: View Class that sent the signal
        :param instance: View Instance that sent the signal
        :param reset_password_token: Token Model Object
        :param args:
        :param kwargs:
        :return:
        POST ${API_URL}/reset_password/ - request a reset password token by using the email parameter
        POST ${API_URL}/reset_password/confirm/ - using a valid token, the users password is set to the provided password
        POST ${API_URL}/reset_password/validate_token/ - will return a 200 if a given token is valid

        """
        # send an e-mail to the user
        token = reset_password_token.key
        token = token[:-1] ##to delete the last character that is '/'
        relativeLink= reverse('password-reset:reset-password-confirm')
        host = instance.request.META['HTTP_ORIGIN']
        #backend_link=host+relativeLink+"?token="+str(token)
        frontend_link=host+"/#/resetpassword?token="+str(token)+"/"

        context = {
            'user': reset_password_token.user,
            'frontend_link': frontend_link,
            'token':token
        }
        # render email text
        email_html_message = render_to_string('email/user_reset_password.html', context)
        email_plaintext_message = render_to_string('email/user_reset_password.txt', context)

        data = {
            "subject": "Initialisation du mot de passe pour {title}".format(title="Togo World Alumni"),
            "text_content":email_plaintext_message,
            "from_email":settings.DEFAULT_FROM_EMAIL,
            "to":[reset_password_token.user.email],
        }
        send_email_alternatives.delay(data,email_html_message) ## to send email in background
