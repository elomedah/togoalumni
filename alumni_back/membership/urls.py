from django.urls import path, include
from . import views

urlpatterns = [
    #path('api/membership/list', views.UserListView.as_view()),
    path('membership/signup/',views.RegisterView.as_view(),name="register"),
    path('membership/login/',views.LoginAPIView.as_view(),name="login"),
    path('membership/logout/',views.LogoutAPIView.as_view(),name="logout"),
    path('membership/email-verify/',views.VerifyEmail.as_view(),name="email-verify"),
    path('membership/profile/',views.UserProfileAPIView.as_view(),name="loggedin-profile"),
    path('membership/profile/<int:pk>',views.UserProfileAPIView.as_view(),name="update-profile"),
    path('membership/profile/etudes',views.StudiesCreateAPIView.as_view(),name="studies-create"),
    path('membership/profile/etudes/<int:pk>',views.StudiesUpdateDeleteAPIView.as_view(),name="studies-update_destroy"),
    path('membership/profile/experience_professionelle',views.ProCareerCreateAPIView.as_view(),name="experience-create"),
    path('membership/profile/experience_professionelle/<int:pk>',views.ProCareerUpdateDeleteAPIView.as_view(),name="experience-update_destroy"),
    path('membership/password-reset/', include('django_rest_passwordreset.urls', namespace='password-reset')),
]
