from rest_framework import serializers
#from .models import Member
#from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
#from rest_framework.validators import UniqueValidator
from .models import User, UserProfile, Studies, ProfessionalCareer
from django.contrib import auth
from rest_framework.exceptions import AuthenticationFailed
from django.utils.text import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

class UserSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(
        write_only=True,
        required=True,
        help_text='',
        style={'input_type': 'password', 'placeholder': 'Password'}
    )
    password2 = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Must be the same as above',
        style={'input_type': 'password', 'placeholder': 'Confirm Password'}
    )

    class Meta:
        model = User
        fields = ('id','username', 'password1','password2', 'email', 'first_name','last_name' )

    def validate(self, data):  ### add rules to validate pwd
        if data['password1'] != data['password2']:
            raise serializers.ValidationError('Passwords must match.')
        return data

    def create(self, validated_data):
        data = {
            key: value for key, value in validated_data.items()
            if key not in ('password1', 'password2')
        }  ### get user fields except those password1 and password 2
        data['password'] = make_password(validated_data.get('password1')) ## django hashes passwords
        return super(UserSerializer, self).create(data)

class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField(max_length=555)

    class Meta:
        model = User
        fields = ['tokens']

class LoginSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255, min_length=3)
    password = serializers.CharField(
        max_length=68, min_length=3, write_only=True)
    username = serializers.CharField(
        max_length=255, min_length=3, read_only=True)


    class Meta:
        model = User
        fields = ['email', 'password', 'username', 'tokens']

    def validate(self, attrs):
        email = attrs.get('email', '')
        password = attrs.get('password', '')
        request = self.context.get("request")

        user = auth.authenticate(email=email, password=password)
        if not user:
            raise AuthenticationFailed('Invalid credentials, try again')
        if not user.is_active:
            raise AuthenticationFailed('Account disabled, contact admin')
        if not user.is_verified:
            raise AuthenticationFailed('Email is not verified')
        return {
            'email': user.email,
            'username': user.username,
            'tokens': user.tokens(),
            'user':user ## to login in views
        }

        return super().validate(attrs)

class StudiesSerializer(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=False, queryset=UserProfile.objects.all())

    class Meta:
        model = Studies
        fields = "__all__"

class ProfessionalCareerSerializer(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=False, queryset=UserProfile.objects.all())

    class Meta:
        model = ProfessionalCareer
        fields = "__all__"

class UserProfileSerializer(serializers.ModelSerializer):
    studies = StudiesSerializer(many=True, required=False)
    pro_career = ProfessionalCareerSerializer(many=True , required=False)
    user = UserSerializer(many=False, read_only=True)

    class Meta:
        model=UserProfile
        fields=['user','pays','phone','statut','date_naissance','description','domaine','studies','pro_career','image']

class PasswordResetSerializer(serializers.ModelSerializer):
    pass
