from django.shortcuts import render
from membership.serializers import UserProfileSerializer
from membership.models import UserProfile
from rest_framework import generics
from rest_framework.filters import SearchFilter



class UserProfileListAPIView(generics.RetrieveAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()

class SearchProfileAPIView(generics.ListAPIView):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    filter_backends = (SearchFilter,)
    search_fields = ['domaine', 'description', 'tags__name', 'user__last_name'
    ,'studies__description', 'studies__diplome', 'studies__university', 'studies__tags__name','pro_career__job_position'
    ,'pro_career__description','pro_career__tags__name']