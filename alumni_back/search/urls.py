from django.urls import path
from . import views

urlpatterns = [
    path('api/search/profile/<int:pk>',views.UserProfileListAPIView.as_view(),name="profile"),
    path('api/search/profile',views.SearchProfileAPIView.as_view(),name="search_profile"),
]