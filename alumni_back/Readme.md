TogoAlumni backend SETUP
==============

## About

It uses:

 - django framework
   https://www.djangoproject.com/

 - django restframework
   https://www.django-rest-framework.org/
   
 - Celery for background tasks
   https://docs.celeryproject.org/en/stable/

 - Redis as a broker
   https://redis.io/

 - postgresql as a database
   https://www.postgresql.org/

## Steps

### To start working on it:
### These steps are given for linux
### For windows, see how to install docker :
https://hub.docker.com/editions/community/docker-ce-desktop-windows

#### 1- clone this repo into your workdir<br/>
git clone https://gitlab.com/elomedah/togoalumni.git
cd togoalumni/alumni_back

#### 2- Insatll all the required packages <br/>
./install_tools.sh you will need sudo rights

#### 3- Build and run docker images <br/>
###     a- Whith django server (developpement)
sudo docker-compose up -d --Build
sudo docker-compose exec web python manage.py migrate
You can see the site at 127.0.0.1:8000
###     b- With gunicorn and nginx (deployement server)
sudo docker-compose -f docker-compose.prod.yml up -d --build
sudo docker-compose -f docker-compose.prod.yml exec web python manage.py migrate
You can see the site at ip(or 127.0.0.1):8081

#### 4- Start playing with the api at endpoints: <br/>
/api/membership/signup  ## Sign Up<br/>
/api/membership/email-verify/?token= ## To verify signed up user <br/>
/api/membership/login ## To login a verified user <br/>
/api/membership/password-reset/ ## Password reset endpoint. The frontend one has to ne /resetpassword?token= <br/>
/api/membership/password-reset/confirm/?token= ## To confirm password reset using sent link <br/>
/api/membership/profile <br>  ## to get all the profiles available in the DB <br>
/api/membership/profile/id  ## to retrieve a profile by its id you can then update or delete it <br>
/api/membership/etudes <br>
/api/membership/etudes/id  ## to retrieve etudes by its id <br>
/api/membership/experience_professionelle <br>
/api/membership/experience_professionelle/id <br>

### To start working on it: Windows

#### 1- create virtualenv:<br/>
`on windows` python -m venv venv

#### 2- activate your virtualenv (from Powershell)<br/>
venv\scripts\activate 

#### 2- activate your virtualenv (from git bash)<br/>
cd envname/scripts<br/>
. activate

#### 3- upgrade pip <br/>
python -m pip install --upgrade pip

#### 4- clone this repo into your workdir<br/>
git clone https://gitlab.com/elomedah/togoalumni.git
cd togoalumni/alumni_back

#### 5- Insatll all the required packages <br/>
pip install -r requirements.txt

#### 6- migrate the database <br/>
python manage.py migrate

#### 7- run the developpement server <br/>
python manage.py runserver (by default it runs on localhost:8000)

That's it.  <br/>
But beware this is just the beginning of a great story
